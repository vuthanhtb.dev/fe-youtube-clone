import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UploadVideoRepository } from './upload-video/UploadVideoRepository';
import { VideoDto } from './video-dto';

@Injectable({
  providedIn: 'root',
})
export class VideoService {
  constructor(private httpClient: HttpClient) {}

  uploadVideo(fileEntry: File): Observable<UploadVideoRepository> {
    const formData = new FormData();
    formData.append('file', fileEntry, fileEntry.name);

    return this.httpClient.post<UploadVideoRepository>(
      'http://localhost:8080/api/videos/',
      formData
    );
  }

  uploadThumbnail(fileEntry: File, id: string): Observable<string> {
    const formData = new FormData();
    formData.append('file', fileEntry, fileEntry.name);
    formData.append('id', id);

    return this.httpClient.post(
      'http://localhost:8080/api/videos/thumbnail',
      formData,
      {
        responseType: 'text',
      }
    );
  }

  getVideo(id: string): Observable<VideoDto> {
    return this.httpClient.get<VideoDto>(
      `http://localhost:8080/api/videos/${id}`
    );
  }

  getAllVideos(): Observable<Array<VideoDto>> {
    return this.httpClient.get<Array<VideoDto>>(
      'http://localhost:8080/api/videos'
    );
  }

  saveVideo(videoMetaData: VideoDto): Observable<VideoDto> {
    return this.httpClient.put<VideoDto>(
      'http://localhost:8080/api/videos',
      videoMetaData
    );
  }

  likeVideo(id: string): Observable<VideoDto> {
    return this.httpClient.post<VideoDto>(
      `http://localhost:8080/api/videos/${id}/like`,
      null
    );
  }

  disLikeVideo(id: string): Observable<VideoDto> {
    return this.httpClient.post<VideoDto>(
      `http://localhost:8080/api/videos/${id}/disLike`,
      null
    );
  }
}
