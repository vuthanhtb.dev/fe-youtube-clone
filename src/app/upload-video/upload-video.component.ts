import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxFileDropEntry } from 'ngx-file-drop';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-upload-video',
  templateUrl: './upload-video.component.html',
  styleUrls: ['./upload-video.component.css']
})
export class UploadVideoComponent implements OnInit {

  public files: NgxFileDropEntry[] = [];
  fileUpload: boolean = false;
  fileEntry: FileSystemFileEntry | undefined;

  constructor(private videoService: VideoService, private router: Router) { }

  ngOnInit(): void {
  }


  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        this.fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        this.fileEntry.file((file: File) => {
          this.fileUpload = true;
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event: any): void {
    console.log(event);
  }

  public fileLeave(event: any): void {
    console.log(event);
  }

  uploadVideo(): void {
    if (undefined !== this.fileEntry) {
      this.fileEntry.file((file) => {
        this.videoService.uploadVideo(file).subscribe((data) => {
          this.router.navigateByUrl(`/save-video-detail/${data.id}`);
        });
      })
    }
  }

}
