export interface UploadVideoRepository {
  id: String;
  url: String;
}
