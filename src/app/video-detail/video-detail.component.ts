import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css'],
})
export class VideoDetailComponent implements OnInit {
  videoId!: string;
  videoUrl!: string;
  videoTitle!: string;
  videoDescription!: string;
  tags: Array<string> = [];
  videoAvailable: boolean = false;
  likeCount: number = 0;
  dislikeCount: number = 0;
  viewCount: number = 0;
  showSubscribeButton: boolean = true;
  showUnSubscribeButton: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private videoService: VideoService,
    private userService: UserService
  ) {
    this.videoId = this.activatedRoute.snapshot.params['id'];
    this.videoService.getVideo(this.videoId).subscribe((data) => {
      this.videoUrl = data.videoUrl;
      this.videoTitle = data.title;
      this.videoDescription = data.description;
      this.tags = data.tags;
      this.videoAvailable = true;
      this.likeCount = data.likes;
      this.dislikeCount = data.dislikes;
      this.viewCount = data.viewCount;
    });
  }

  ngOnInit(): void {}

  likeVideo(): void {
    this.videoService.likeVideo(this.videoId).subscribe((data) => {
      this.likeCount = data.likes;
      this.dislikeCount = data.dislikes;
    });
  }

  disLikeVideo(): void {
    this.videoService.disLikeVideo(this.videoId).subscribe((data) => {
      this.likeCount = data.likes;
      this.dislikeCount = data.dislikes;
    });
  }

  subscribeToUser(): void {
    let userId = this.userService.getUserId();
    console.log(userId);
    this.userService.subscribeToUser(userId).subscribe((data) => {
      this.showUnSubscribeButton = true;
      this.showSubscribeButton = false;
    });
  }

  unSubscribeToUser(): void {
    let userId = this.userService.getUserId();
    this.userService.unSubscribeUser(userId).subscribe((data) => {
      this.showUnSubscribeButton = false;
      this.showSubscribeButton = true;
    });
  }
}
